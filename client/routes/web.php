<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use http\Exception\InvalidArgumentException;
use Illuminate\Support\Facades\Http;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/login',function(Request $request){
    //random state parameter for blocking CSRF attack
    $request->session()->put('state',$state = Str::random(40));
    $query = http_build_query([
       "client_id" => "93cae4c3-273b-4fb6-b706-fa442894fbe5",
       "redirect_uri" => "http://127.0.0.1:8080/callback",
       "response_type" => "code",
       "scope" => "view-user",
       "state" => $state
    ]);
    return redirect("http://127.0.0.1:8000/oauth/authorize?". $query);
});

Route::get('/callback',function(Request $request){
    $state = $request->session()->pull('state');
    throw_unless(strlen($state)>0 && $state == $request->state,InvalidArgumentException::class);
    $response = Http::asForm()->post('http://127.0.0.1:8000/oauth/token',
        [
            'client_id' => '93cae4c3-273b-4fb6-b706-fa442894fbe5',
            'client_secret' => 'N7JRLPlDbSqhgk6zF7K8DbctQmdn5L7XGKXJ034U',
            'redirect_uri' => 'http://127.0.0.1:8080/callback',
            'grant_type' => 'authorization_code',
            'code' => $request->code
        ]);
    $request->session()->put($response->json());
    return redirect('/authuser');
});

Route::get('/authuser',function(Request $request){
    $access_token = $request->session()->get('access_token');
    $response = Http::withHeaders([
        "Accept" => 'application/json',
        "Authorization" => 'Bearer '.$access_token
    ])->get('http://127.0.0.1:8000/api/user');
    return $response->json();
});
